package com.example.srish.amoledify.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.srish.amoledify.R;

public class MainActivity extends AppCompatActivity
{

    protected int _splashTime = 5000;
    ImageView imageViewLogo, imageViewText;
    Animation animationSlideUp;
    Animation animationFadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewLogo = findViewById(R.id.imageViewLogo);
        imageViewText = findViewById(R.id.imageViewText);

        animationSlideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up_anim);
        imageViewLogo.startAnimation(animationSlideUp);


        animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        imageViewText.startAnimation(animationFadeIn);

        Thread splashTread;


        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(_splashTime);
                    }

                } catch (InterruptedException e) {
                } finally {
                    finish();

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    // "login","no" means default string value is "no", so if you didn't set yes after login, it will be no as default
                    if(preferences.getString("login", "no").equals("yes")){
                        //login value is yes, so start mainactivity

                        Intent i = new Intent();
                        i.setClass(MainActivity.this, SignInProfile.class);
                        startActivity(i);
                    }
                    else{
                        //login value is no, so start loginactivity

                        Intent i = new Intent();
                        i.setClass(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                    }

                    //stop();
                }
            }
        };

        splashTread.start();
    }

}


