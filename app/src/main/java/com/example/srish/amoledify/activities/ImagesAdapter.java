package com.example.srish.amoledify.activities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.srish.amoledify.R;

import java.util.List;

/**
 * Created by srish on 10-03-2018.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder> {

    List<Image> imageList;
    Context mCtx;

    public ImagesAdapter(List<Image> imageList, Context mCtx) {
        this.imageList = imageList;
        this.mCtx = mCtx;
    }

    @Override
    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_images, null);
        ImagesViewHolder holder = new ImagesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ImagesViewHolder holder, int position) {

//        holder.textView.setText();


    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ImagesViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView textView;

        public ImagesViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
//            textView = itemView.findViewById(R.id.textView);
        }
    }
}
