package com.example.srish.amoledify.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.srish.amoledify.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by srish on 10-03-2018.
 */

public class FragmentImages extends Fragment {

    RecyclerView recyclerView;
    ImagesAdapter imagesAdapter;

    List<Image> imageList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_images, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        recyclerView = view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        imageList = new ArrayList<>();

        imageList.add(new Image("",""));
        imageList.add(new Image("",""));
        imageList.add(new Image("",""));
        imageList.add(new Image("",""));
        imageList.add(new Image("",""));
        imageList.add(new Image("",""));


        imagesAdapter = new ImagesAdapter(imageList, getActivity());

        recyclerView.setAdapter(imagesAdapter);
    }

}
