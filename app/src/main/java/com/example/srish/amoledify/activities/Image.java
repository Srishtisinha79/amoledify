package com.example.srish.amoledify.activities;

/**
 * Created by srish on 10-03-2018.
 */

public class Image {
    String imageUrl;
    String desc;

    public Image(String imageUrl, String desc) {
        this.imageUrl = imageUrl;
        this.desc = desc;
    }
}
